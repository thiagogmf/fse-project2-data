#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>

#define OUTFILE "out_julia_gpu.bmp"

const char* kernelSource =
"__kernel void computeJuliaFractal(__global unsigned char* pixelArray) {\n"
"    int x = get_global_id(0);\n"
"    int y = get_global_id(1);\n"
"\n"
"    // Image properties\n"
"    int width = get_global_size(0);\n"
"    int height = get_global_size(1);\n"
"\n"
"    // Julia set properties\n"
"    float xMin = -1.6f;\n"
"    float xMax = 1.6f;\n"
"    float yMin = -0.9f;\n"
"    float yMax = 0.9f;\n"
"    float juliaReal = -0.79f;\n"
"    float juliaImaginary = 0.15f;\n"
"    int maxIterations = 300;\n"
"\n"
"    // Compute coordinates\n"
"    float floatY = (yMax - yMin) * (float)y / height + yMin;\n"
"    float floatX = (xMax - xMin) * (float)x / width + xMin;\n"
"\n"
"    // Initialize variables\n"
"    float real = floatY;\n"
"    float imaginary = floatX;\n"
"    int numIterations = maxIterations;\n"
"\n"
"    // Compute the complex series convergence\n"
"    while ((imaginary * imaginary + real * real < 2 * 2) && (numIterations > 0)) {\n"
"        float temp = imaginary * imaginary - real * real + juliaReal;\n"
"        real = 2 * imaginary * real + juliaImaginary;\n"
"        imaginary = temp;\n"
"        numIterations--;\n"
"    }\n"
"\n"
"    // Paint pixel based on the number of iterations\n"
"    float colorBias = (float)numIterations / maxIterations;\n"
"    unsigned char red = (numIterations == 0 ? 200 : -500.0f * pow(colorBias, 1.6));\n"
"    unsigned char green = (numIterations == 0 ? 100 : -255.0f * pow(colorBias, 0.3));\n"
"    unsigned char blue = (numIterations == 0 ? 100 : 255 - 255.0f * pow(colorBias, 3.0));\n"
"\n"
"    // Set pixel values in the pixel array\n"
"    int index = (y * width + x) * 3;\n"
"    pixelArray[index] = red;\n"
"    pixelArray[index + 1] = green;\n"
"    pixelArray[index + 2] = blue;\n"
"}\n";

int write_bmp_header(FILE *f, int width, int height) {
    unsigned int rowSizeInBytes = width * 3 + ((width * 3) % 4 == 0 ? 0 : (4 - (width * 3) % 4));

    // Define all fields in the BMP header
    char id[2] = "BM";
    unsigned int fileSize = 54 + (int)(rowSizeInBytes * height * sizeof(unsigned char));
    short reserved[2] = { 0, 0 };
    unsigned int offset = 54;

    unsigned int size = 40;
    unsigned short planes = 1;
    unsigned short bits = 24;
    unsigned int compression = 0;
    unsigned int imageSize = width * height * 3 * sizeof(unsigned char);
    int xRes = 0;
    int yRes = 0;
    unsigned int nColors = 0;
    unsigned int importantColors = 0;

    // Write the bytes to the file, keeping track of the number of written "objects"
    size_t ret = 0;
    ret += fwrite(id, sizeof(char), 2, f);
    ret += fwrite(&fileSize, sizeof(int), 1, f);
    ret += fwrite(reserved, sizeof(short), 2, f);
    ret += fwrite(&offset, sizeof(int), 1, f);
    ret += fwrite(&size, sizeof(int), 1, f);
    ret += fwrite(&width, sizeof(int), 1, f);
    ret += fwrite(&height, sizeof(int), 1, f);
    ret += fwrite(&planes, sizeof(short), 1, f);
    ret += fwrite(&bits, sizeof(short), 1, f);
    ret += fwrite(&compression, sizeof(int), 1, f);
    ret += fwrite(&imageSize, sizeof(int), 1, f);
    ret += fwrite(&xRes, sizeof(int), 1, f);
    ret += fwrite(&yRes, sizeof(int), 1, f);
    ret += fwrite(&nColors, sizeof(int), 1, f);
    ret += fwrite(&importantColors, sizeof(int), 1, f);

    // Success means that we wrote 17 "objects" successfully
    return (ret != 17);
}

int main(int argc, char* argv[]) {
    if (argc <= 1 || atoi(argv[1]) < 1) {
        fprintf(stderr, "Enter 'N' as a positive integer!\n");
        return -1;
    }

    int n = atoi(argv[1]);
    int width = 2 * n;
    int height = n;
    int imageSize = width * height * 3;

    unsigned char* pixelArray = (unsigned char*)calloc(imageSize, sizeof(unsigned char));

    cl_platform_id platform;
    cl_device_id device;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;

    cl_int err;

    // Select platform and device
    err = clGetPlatformIDs(1, &platform, NULL);
    err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);

    // Create context
    context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);

    // Create command queue
    queue = clCreateCommandQueueWithProperties(context, device, 0, &err);

    // Create program
    program = clCreateProgramWithSource(context, 1, &kernelSource, NULL, &err);

    // Build program
    err = clBuildProgram(program, 1, &device, NULL, NULL, NULL);

    // Create kernel
    kernel = clCreateKernel(program, "computeJuliaFractal", &err);

    // Create buffer for pixel array
    cl_mem pixelArrayBuffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, imageSize * sizeof(unsigned char), NULL, &err);

    // Set kernel arguments
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &pixelArrayBuffer);

    // Define work sizes
    size_t globalSize[2] = { width, height };

    // Enqueue kernel
    err = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, globalSize, NULL, 0, NULL, NULL);

    // Read buffer
    err = clEnqueueReadBuffer(queue, pixelArrayBuffer, CL_TRUE, 0, imageSize * sizeof(unsigned char), pixelArray, 0, NULL, NULL);

    // Release resources
    clReleaseMemObject(pixelArrayBuffer);
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);

    // Write pixel array to BMP file
    FILE* output_file = fopen(OUTFILE, "wb");
    write_bmp_header(output_file, width, height);
    fwrite(pixelArray, sizeof(unsigned char), imageSize, output_file);
    fclose(output_file);

    // Free memory
    free(pixelArray);

    return 0;
}

